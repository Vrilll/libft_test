OBJ_DIR = build
SRC_DIR = tests
CFLAGS = -Wall -Wextra -I$(HOME)/.brew/include -Ilibft/include 
CLIBS = -L$(HOME)/.brew/lib -Llibft -lcriterion -lft
OBJS := $(addprefix $(OBJ_DIR)/test_, atoi.o itoa.o mem.o ft_str.o stdstr.o file.o)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	cc $(CFLAGS) -c -o $@ $<

all: test

test: $(OBJS)
	make -j4 -C libft
	clang $(CFLAGS) $(CLIBS) -o $@ $^

$(OBJS): $(OBJ_DIR)

$(OBJ_DIR):
	@mkdir $(OBJ_DIR)

clean:
	@rm -rf build
	@rm -f test

re: clean all
