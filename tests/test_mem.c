/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_mem.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 15:00:17 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/31 16:05:12 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <stdlib.h>

Test(ft_mem, calloc)
{
	cr_assert_arr_eq(ft_calloc(0, 0), calloc(0, 0), 0);
	cr_assert_arr_eq(ft_calloc(5, 0), calloc(5, 0), 0);
	cr_assert_arr_eq(ft_calloc(100, sizeof(int)), calloc(100, sizeof(int)),
			100 * sizeof(int));
}

Test(ft_mem, memset)
{
	char *str1, *str2;
	size_t str_len;

	str1 = strdup("Test string");
	str2 = strdup(str1);
	str_len = strlen(str1);
	cr_assert_eq(ft_memset(str1, 'A', 5), str1);
	memset(str2, 'A', 5);
	cr_assert_str_eq(str1, str2);
}

Test(ft_mem, memcpy)
{
	char *str1, *str2, *str3;
	size_t str_len;

	str1 = strdup("Test\0 string");
	str_len = 13;
	str2 = calloc(str_len, sizeof(char));
	str3 = calloc(str_len, sizeof(char));
	cr_assert_eq(ft_memcpy(str2, str1, 3), str2);
	memcpy(str3, str1, 3);
	cr_assert_arr_eq(str2, str3, str_len);
	memcpy(str3, str1, str_len);
	ft_memcpy(str2, str1, str_len);
	cr_assert_arr_eq(str2, str3, str_len);
}

Test(ft_mem, memccpy)
{
	char *str1, *str2, *str3;
	size_t str_len;

	str1 = strdup("Test\0 string");
	str_len = 13;
	str2 = calloc(str_len, sizeof(char));
	str3 = calloc(str_len, sizeof(char));
	cr_assert_eq(ft_memccpy(str2, str1, 0, str_len), str2 + 5);
	memccpy(str3, str1, 0, str_len);
	cr_assert_arr_eq(str2, str3, str_len);
	cr_assert_null(ft_memccpy(str2, str1, 'Z', str_len));
	memccpy(str3, str1, 'Z', str_len);
	cr_assert_arr_eq(str2, str3, str_len);
}

Test(ft_mem, memmove)
{
	char *str1, *str2;
	size_t str_len;

	str1 = strdup("Test string");
	str2 = strdup(str1);
	str_len = strlen(str1 + 1);
	cr_assert_eq(ft_memmove(str1, str1 + 1, str_len - 1), str1);
	memmove(str2, str2 + 1, str_len - 1);
	cr_assert_arr_eq(str1, str2, str_len);
	cr_assert_eq(ft_memmove(str1 + 1, str1, 5), str1 + 1);
	memmove(str2 + 1, str2, 5);
	cr_assert_arr_eq(str1, str2, str_len);
}

Test(ft_mem, memchr)
{
	char *str;
	size_t len;

	str = "This is a\0 string";
	len = sizeof(char) * 18;
	cr_assert_eq(memchr(str, '\0', len), ft_memchr(str, '\0', len));
	cr_assert_eq(memchr(str, 's', len), ft_memchr(str, 's', len));
	cr_assert_null(ft_memchr(str, 'z', len));
}

Test(ft_mem, bzero)
{
	char *str, *str2;
	size_t len;

	len = 10;
	str = malloc(sizeof(char) * len);
	str2 = malloc(sizeof(char) * len);
	memcpy(str, "Test\0Test", len);
	memcpy(str2, str, len);
	bzero(str, 2);
	ft_bzero(str2, 2);
	cr_assert_arr_eq(str, str2, len);
	bzero(str, len);
	ft_bzero(str2, len);
	cr_assert_arr_eq(str, str2, len);
}

Test(ft_mem, memcmp)
{
	char *s1, *s2;
	size_t len;

	s1 = "me\0rp";
	s2 = "me\trp";
	len = 6;
	cr_assert_eq(memcmp(s1, s2, len), ft_memcmp(s1, s2, len));
	cr_assert_eq(memcmp(s2, s1, len), ft_memcmp(s2, s1, len));
	s2 = "me\0rp\0o";
	cr_assert_eq(memcmp(s2, s1, len), ft_memcmp(s2, s1, len));
}
