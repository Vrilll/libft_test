/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_itoa.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 14:51:11 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/30 14:59:23 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <limits.h>

Test(ft_itoa, all)
{
	cr_expect_str_eq(ft_itoa(INT_MIN), "-2147483648");
	cr_expect_str_eq(ft_itoa(INT_MAX), "2147483647");
	cr_expect_str_eq(ft_itoa(0), "0");
	cr_expect_str_eq(ft_itoa(-1001), "-1001");
	cr_expect_str_eq(ft_itoa(10501), "10501");
}
