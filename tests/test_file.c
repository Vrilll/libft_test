/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_file.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/31 17:39:24 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/31 19:02:32 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>

Test(ft_fd, putstr)
{
	int fd;
	char *buf;

	fd = open("putstr.tmp", O_RDWR | O_CREAT, 0000777);
	ft_putendl_fd("Hello world!", fd);
	close(fd);
	buf = malloc(sizeof(char) * 32);
	fd = open("putstr.tmp", O_RDWR);
	read(fd, buf, 32);
	cr_assert_str_eq(buf, "Hello world!\n");
	close(fd);
	unlink("putstr.tmp");
}

Test(ft_fd, putnbr)
{
	int fd;
	char *buf;

	fd = open("putnbr.tmp", O_RDWR | O_CREAT, 0000777);
	ft_putnbr_fd(-1337, fd);
	close(fd);
	buf = malloc(sizeof(char) * 8);
	fd = open("putnbr.tmp", O_RDWR);
	read(fd, buf, 8);
	cr_assert_str_eq(buf, "-1337");
	close(fd);
}
