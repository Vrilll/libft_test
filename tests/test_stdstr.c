/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_stdstr.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/31 16:13:19 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/31 17:33:10 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <stdlib.h>

Test(ft_stdstr, strlen)
{
	char *empty, *str, *nullstr;

	empty = "";
	str = "Hello world!";
	nullstr = "Hello\0world!";
	cr_assert_eq(strlen(empty), ft_strlen(empty));
	cr_assert_eq(strlen(str), ft_strlen(str));
	cr_assert_eq(strlen(nullstr), ft_strlen(nullstr));
}

Test(ft_stdstr, strlcpy)
{
	char *str1, *str2, *str3;
	size_t str_len;

	str1 = "Source string!";
	str_len = strlen(str1);
	str2 = calloc(str_len, sizeof(char));
	str3 = calloc(str_len, sizeof(char));
	cr_assert_eq(ft_strlcpy(str3, str1, 4), strlcpy(str2, str1, 4));
	cr_assert_arr_eq(str2, str3, str_len);
	cr_assert_eq(ft_strlcpy(str3, str1, str_len), strlcpy(str2, str1, str_len));
	cr_assert_arr_eq(str2, str3, str_len);
}

Test(ft_stdstr, strchr)
{
	char *str;

	str = "This is a\0 string";
	cr_assert_eq(strchr(str, 'i'), ft_strchr(str, 'i'));
	cr_assert_null(ft_strchr(str, 'g'));
}

Test(ft_stdstr, strrchr)
{
	char *str;

	str = "This is a\0 string";
	cr_assert_eq(strrchr(str, 'i'), ft_strrchr(str, 'i'));
	cr_assert_null(ft_strrchr(str, 'g'));
}

Test(ft_stdstr, strnstr)
{
	char *str;
	size_t len;

	str = "The quick brown fox jumped over the lazy \0dog.";
	len = strlen(str) + 5;
	cr_assert_eq(strnstr(str, "fox", len), ft_strnstr(str, "fox", len));
	cr_assert_eq(strnstr(str, "", len), ft_strnstr(str, "", len));
	cr_assert_eq(strnstr(str, "dog.", len), ft_strnstr(str, "dog.", len));
}

Test(ft_stdstr, strdup)
{
	cr_assert_str_eq(strdup("Hi"), ft_strdup("Hi"));
	cr_assert_str_eq(strdup("H\0i"), ft_strdup("H\0i"));
	cr_assert_str_eq(strdup(""), ft_strdup(""));
}

Test(ft_stdstr, strncmp)
{
	char *s1, *s2;
	size_t len;

	s1 = "merp";
	s2 = "morp";
	len = 5;
	cr_assert_eq(strncmp(s1, s2, len), ft_strncmp(s1, s2, len));
	cr_assert_eq(strncmp(s2, s1, len), ft_strncmp(s2, s1, len));
	s2 = "merp";
	cr_assert_eq(strncmp(s2, s1, len), ft_strncmp(s2, s1, len));
}

Test(ft_stdstr, issomething)
{
	char alpha, digit, ascii, print;

	alpha = 'a';
	digit = '1';
	ascii = '\t';
	print = ' ';
	cr_expect_eq(isalpha(alpha), ft_isalpha(alpha));
	cr_expect_eq(isdigit(digit), ft_isdigit(digit));
	cr_expect_eq(isascii(ascii), ft_isascii(ascii));
	cr_expect_eq(isprint(print), ft_isprint(print));
	cr_expect_eq(isalpha(digit), ft_isalpha(digit));
	cr_expect_eq(isdigit(alpha), ft_isdigit(alpha));
	cr_expect_eq(isascii(-5), ft_isascii(-5));
	cr_expect_eq(isprint('\0'), ft_isprint('\0'));
	cr_expect_eq(isalnum(alpha), ft_isalnum(alpha));
	cr_expect_eq(isalnum(digit), ft_isalnum(digit));
	cr_expect_eq(isalnum(print), ft_isalnum(print));
}

Test(ft_stdstr, caseswitch)
{
	char upper,lower,invalid;

	upper = 'A';
	lower = 'a';
	invalid = '\n';
	cr_expect_eq(ft_toupper(lower), upper);
	cr_expect_eq(ft_tolower(upper), lower);
	cr_expect_eq(ft_toupper(invalid), invalid);
	cr_expect_eq(ft_tolower(invalid), invalid);
}
