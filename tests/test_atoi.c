/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_atoi.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 14:23:39 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/31 20:25:12 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <limits.h>
#include <string.h>
#include <stdio.h>

Test(ft_atoi, basic_op)
{
	cr_expect(ft_atoi("1") == 1);
	cr_expect(ft_atoi("0") == 0);
	cr_expect(ft_atoi("-1") == -1);
}

Test(ft_atoi, max_value)
{
	cr_expect(ft_atoi("-2147483648") == INT_MIN);
	cr_expect(ft_atoi("2147483647") == INT_MAX);
	cr_expect(ft_atoi("200000000000") == atoi("200000000000"));
	printf("%d\n", atoi("200000000000"));
}

Test(ft_atoi, malformed_input)
{
	cr_expect(ft_atoi("10g5") == 10);
	cr_expect(ft_atoi("ABC1") == 0);
	cr_expect(ft_atoi("++1") == 0);
	cr_expect(ft_atoi("--1") == 0);
}

Test(ft_atoi, signs)
{
	cr_expect(ft_atoi("-1") == -1);
	cr_expect(ft_atoi("+1") == 1);
	cr_expect(ft_atoi("+1337") == 1337);
}
