/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   test_ft_str.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: tjans <marvin@codam.nl>                      +#+                     */
/*                                                   +#+                      */
/*   Created: 2019/10/30 15:44:33 by tjans         #+#    #+#                 */
/*   Updated: 2019/10/31 17:38:28 by tjans         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <libft.h>
#include <stdlib.h>

Test(ft_str, substr)
{
	cr_assert_str_eq(
			ft_substr("Hello World!", 6, 6),
			"World!");
	cr_assert_str_eq(
			ft_substr("Hello World!", 6, 5),
			"World");
	cr_assert_str_eq(
			ft_substr("Hello World!", 6, 100),
			"World!");
	cr_assert_str_empty(ft_substr("", 0, 100));
}

Test(ft_str, strjoin)
{
	cr_assert_str_empty(ft_strjoin("", ""));
	cr_assert_str_eq(
			ft_strjoin("Hello ", "World!"),
			"Hello World!");
}

Test(ft_str, strtrim)
{
	cr_assert_str_empty(
			ft_strtrim("ABC!\t\n", "BAC\n\t!"));
	cr_assert_str_eq(
			ft_strtrim("Hello\n# World!", "h\n#"),
			"Hello World!");
}

Test(ft_str, split)
{
	char	*str;
	char	**arr;

	str = "Hello World";
	arr = ft_split(str, ' ');
	cr_assert_str_eq(arr[0], "Hello");
	cr_assert_str_eq(arr[1], "World");
	cr_assert_null(arr[2]);
}

Test(ft_str, split_whitespace)
{
	char	*str;
	char	**arr;

	str = "!!Hello!World!";
	arr = ft_split(str, '!');
	cr_assert_str_eq(arr[0], "Hello");
	cr_assert_str_eq(arr[1], "World");
	cr_assert_null(arr[2]);
}

Test(ft_str, split_empty)
{
	char	*str;
	char	**arr;

	str = "!!!!";
	arr = ft_split(str, '!');
	cr_assert_null(arr[0]);
}

char test_fun(unsigned int i, char c)
{
	static unsigned int cnt = 0;

	cr_expect_eq(cnt, i);
	cnt++;
	return (tolower(c));
}

Test(ft_str, ft_strmapi)
{
	char *str;

	str = "UPPER";
	cr_assert_str_eq(ft_strmapi(str, &test_fun), "upper");
}
